
using RelevanceStacktrace
#%%

using Revise

includet("../src/GraphPipe.jl")

# using .GraphFileMeta: op_str2symbol
# println(op_str2symbol)
# #%%

using .GraphPipe: graph_ref

d_graph = graph_ref( (data_graph_name = "crypto_graph/initial.2.json",));

println(d_graph)

#%%
using Test

@inferred graph_ref(ctx)
