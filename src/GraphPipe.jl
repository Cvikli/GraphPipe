

module GraphPipe

using Revise

include("GraphPrmsFunc.jl")
include("GraphWorld.jl")
include("GraphFunc.jl")
include("GraphFileMeta.jl")
include("GraphMethods.jl")


using .GraphFileMeta
using .GraphMethods: convert2Graph, get_raw_graph
using Storage: loader
# using InteractiveUtils

try
# includet("_8_python/python_api.jl")
# import ..DTPyApi: load_model_meta_by_modelID, get_preprocessed_graph_data, get_input_labels
catch
	@warn "Python API could not be loaded."
end

function graph_ref(context::NamedTuple; prefix="_2_graphsets/")
	if (haskey(context, :data_graph_name))
		return graph_ref(context.data_graph_name, prefix=prefix)
	end
	if (haskey(context, :model_id))
		model_id = context.model_id
		# meta = load_model_meta_by_modelID(model_id)
		# @show meta
		graph_data = get_preprocessed_graph_data(model_id)
		graph = graph_ref(graph_data)
		graph.loss_fn = "diabtrend"
		return graph, () -> println("SAVE UNIMPLEMENTED")
	end
	@assert false
end

function graph_ref(file_name::String; storage_type::Symbol=:FILE, what::String="Model", allow_warnings=false, prefix="_2_graphsets/")
	save_fn, load_fn, data_name = loader(prefix * file_name, storage_type)
	# @show load_fn(what)
	dd = load_fn(what)  # TODO has Core.Box (fix this before tackling other problems, see https://timholy.github.io/SnoopCompile.jl/stable/snoopr/#Fixing-Core.Box)
	raw_graph = get_raw_graph(hasproperty(dd, :is_learnable) ? dd.is_learnable : false,  # TODO has Core.Box (fix this before tackling other problems, see https://timholy.github.io/SnoopCompile.jl/stable/snoopr/#Fixing-Core.Box)
												hasproperty(dd, :loss_fn) ? dd.loss_fn : "mse",
												hasproperty(dd, :last_edge_id) ? dd.last_edge_id : 0,
												hasproperty(dd, :edges) ? dd.edges : [],
												hasproperty(dd, :last_node_id) ? dd.last_node_id : 0,
												hasproperty(dd, :nodes) ? dd.nodes : [])
	@assert raw_graph !== nothing "Initial graph should not be nothing."
	graph = convert2Graph(raw_graph, allow_warnings=allow_warnings, verbose=true)

	# print(f"Graph Loaded: {timer.elapsed():2.5f}")
	return graph
end


function graph_ref(firebase_graph::Dict{Any, Any})
	mapping = get_input_labels()
	graph_data = firebase_graph["graph"]
	edges, nodes = [], []
	for (k, v) in graph_data
		append!(edges, [[edge_st, k] for edge_st in v["in"]])
		if startswith(v["label"], "output")
			slice = parse(Int32, v["label"][8:end])
			push!(nodes, [k, "Output", (index=0, slicer=[nothing, [slice, slice + 1]])])
		elseif haskey(mapping, v["label"])
			slice = mapping[v["label"]]
			push!(nodes, [k, "Input", (index=0, slicer=[nothing, [slice, slice + 1]])])
		else
			push!(nodes, [k, v["label"]])
		end
	end
	max_id = maximum([k for (k,v) in graph_data])
	raw_graph = get_raw_graph(true, "mse", size(edges, 1) + 1, edges, max_id, nodes)
	graph = convert2Graph(raw_graph, allow_warnings=false, verbose=true)
	
	graph
end

precompile(graph_ref, (String,))
precompile(graph_ref, (Dict{Any,Any},))
precompile(graph_ref, (NamedTuple{(:data_name, :train_ratio, :data_graph_name), Tuple{String, Float64, String}},))
precompile(graph_ref, (NamedTuple{(:data_name, :train_ratio, :data_graph_name), Tuple{String, Float64, String}},String))

end
