
module GraphMethods

using ..GraphFileMeta
using ..GraphFunc
using ..GraphWorld: GraphRaw, Graph, Node, Options, GPatch, empty_prealloc

using InteractiveUtils
using Flux: relu, stack
using EllipsisNotation
using SparseArrays
using Boilerplate: sizes, filter, @sizes, @typeof


get_raw_graph(is_learnable, loss_fn, edge_id, edge, node_id, nodes) = GraphRaw(is_learnable, loss_fn, edge_id, edge, node_id, nodes)
function convert2Graph(g_raw::GraphRaw; allow_warnings::Bool = false, verbose::Bool=false)
  size = length(g_raw.nodes)
  g = Graph(nodes = Vector{Node}(undef, size),
            matrix = spzeros(Int, size, size),
            loss_fn = g_raw.loss_fn,
            is_learnable = g_raw.is_learnable)
  nid2index::Dict{Int32,Int32} = Dict{Int32,Int32}()
  for (pos, n) in enumerate(g_raw.nodes)
    symbol = GraphFileMeta.op_str2symbol[n[2]]
    op = GraphFunc.forward_backward_2_fluxop[symbol]
    # op = GraphFunc.op_2_fluxop[symbol]
    if length(n) == 3
      @assert n[3].slicer[1] === nothing "Unhandled slicing case."
      options = (;sliceStart=n[3].slicer[end][1])
      node = Node(; id = n[1], symbol = symbol, options = Options(;Dict(pairs(options))...), op = op)
    else
      node = Node(; id = n[1], symbol = symbol, op = op)
    end
    nid2index[node.id] = pos
    g.nodes[pos] = node
  end
  for e in g_raw.edges
    pos_in, pos_out = nid2index[e[1]], nid2index[e[2]]
    g.matrix[pos_in, pos_out] += 1
  end
  for (i, n) in enumerate(g.nodes)
    inp_count = sum(g.matrix[:, i])
  end
  g.states = filter(GraphFileMeta.is_state, g.nodes)
  g.is_rnn = length(g.states) > 0
  # @code_warntype order!(g)
  res = order!(g, verbose)
  return res || allow_warnings ? g : Graph()
end
prm_count(g::Graph) = g.nodes |> filter(GraphFileMeta.is_var) |> length
to_index(d) = map(n -> n.index, d)
filter_outputs(d) = filter((n) -> n.symbol == :out, d)
function order!(g::Graph, verbose::Bool=false)
  # cut matrixes which can't reach output.
  init_outsize = length(g.nodes |> filter_outputs)
  # cut nodes connecting into input nodes.
  nodes_out = [(n.symbol in [:inp, :prm, :const] ? 1 : 0) for n in g.nodes]
  wrong_nodes = bfs_noblock(g, nodes_out, reverse = true, exclude_start=true)
  good_nodes = setdiff(1:length(g.nodes), wrong_nodes)
  reorder!(g, good_nodes)

  # cut nodes cannot reach end node. States which doesn't effect output can be cut.
  nodes_out = [(n.symbol in [:out, :inp] ? 1 : 0) for n in g.nodes]
  reach_indices = bfs_noblock(g, nodes_out, reverse = true)
  reorder!(g, reach_indices)

  # cut nonstarting branches. Also keep :out, because noop transformation.
  nodes_out = [(n.symbol in [:inp, :prm, :const, :state, :zstate, :out] ? 1 : 0) for n in g.nodes]
  working_node_indices = bfs_noblock(g, nodes_out, verbose=true)
  reorder!(g, working_node_indices)

  # reorder
  nodes_in = [(n.symbol in [:inp, :prm, :const, :state, :zstate] ? 1 : 0) for n in g.nodes]
  order_map = bfs(g, nodes_in)
  reorder!(g, order_map)
  nodes_num = length(g.nodes)

  expected_nodes_inputs_num = sum(g.matrix, dims = 1)[1, :]
  min_nodes_inputs_num = Int[GraphFileMeta.min_op_input_num[n.symbol] for n in g.nodes]
  merged_input_conditions = [min_nodes_inputs_num expected_nodes_inputs_num]
  graph_based_expected_inputs_num = maximum(merged_input_conditions, dims = 2)[:, 1]
  no_nodes = zeros(Float64, size(min_nodes_inputs_num))
  input_nodes = (no_nodes .== graph_based_expected_inputs_num)
  # state_nodes_mask = onp.array(onp.vectorize(lambda n: n.op in Op_States)(self.nodes))
  state_nodes = g.nodes .|> GraphFileMeta.is_state
  graph_based_expected_inputs_num[state_nodes] .= 0
  # graph_based_expected_inputs_num.at[state_nodes > 0].set(0)
  graph_based_expected_inputs_without_state_num = graph_based_expected_inputs_num
  acycled_matrix = copy(g.matrix)#[:, :]
  acycled_matrix[:, state_nodes] .= 0
  # acycled_matrix.at[:, state_nodes > 0].set(0)
  active_nodes = Int32.(input_nodes .| state_nodes)
  fired_nodes = similar(active_nodes)
  change = similar(input_nodes)
  acycled_matrix_tr = acycled_matrix'
  while true
    last_active_nodes = copy(active_nodes)
    fired_nodes .= acycled_matrix_tr * active_nodes
    active_nodes .= fired_nodes .== graph_based_expected_inputs_without_state_num
    change .= last_active_nodes .⊻ active_nodes
    if !any(change)
      break
    end
  end
  # self.matrix_input_indices = onp.array(onp.ones(self.size), dtype=onp.object)
  node_input_indices = Vector{Union{Nothing, Vector{Int32}}}(undef, nodes_num)
  # node_output_indices = Vector{Union{Nothing, Vector{Int32}}}(undef, nodes_num)
  for i = 1:nodes_num
    column = g.matrix[:, i]
    node_input_indices[i] = findall(x -> x > 0, column) # |> l -> length(l) == 0 ? nothing : l
    # row = g.matrix[i, :]
    # node_output_indices[i] = findall(x -> x > 0, row) |> v -> length(v) == 0 ? nothing : v
  end
  g.node_input_indices = node_input_indices
  # g.node_output_indices = node_output_indices
  var_counter::Int = 1
  for (i, n) in enumerate(g.nodes)
    if GraphFileMeta.is_state_learnable(n)
      n.var_index = var_counter
      var_counter += 1
    elseif GraphFileMeta.is_var(n)
      n.var_index = var_counter
      var_counter += 1
    end
    n.index = i
  end
  out_idx = g.nodes |> filter_outputs |> to_index
  if g.is_rnn && length(g.states) == 0
    verbose && println("All states got cut out.")
    return false
  end
  if length(out_idx) != init_outsize
    verbose && println("Output node got cut!")
    return false
  end
  g.out_idx = out_idx

  max_nodes_inputs_num = Int[GraphFileMeta.max_op_input_num[n.symbol] for n in g.nodes]
  if all(active_nodes .== 1) && all(fired_nodes .<= max_nodes_inputs_num)
    return true
  end
  # show(g.nodes[active_nodes .== 0])
  # @show g.out_idx
  # @show g.nodes[g.out_idx]
  if all(active_nodes[g.out_idx] .!= 0)
    return true
  end
  verbose && @warn "ERROR. Output not connected."
  return false
end
# function precalc_graph(g::Graph)
#   for patch in g.patches

#   end
# end
function to_patch_format(g::Graph)
  g.prealloc = empty_prealloc()
  g.nodes = Node[]
  g.transforms = g.transforms
end

function bfs_noblock(g::Graph, init_nodes; reverse = false, exclude_start = false, verbose=false)
  order_map = []
  can_reach = init_nodes
  change = exclude_start ? [] : can_reach .!= 0

  connect_matrix = reverse ? g.matrix : g.matrix'
  while true
    for (i, v) in enumerate(change)
      v == true && push!(order_map, i)
    end
    last_active_nodes = can_reach
    activated = connect_matrix * can_reach
    can_reach = can_reach .+ activated
    can_reach = min.(can_reach, 1)
    change = last_active_nodes .!== can_reach
    if !any(change)
      break
    end
  end
  order_map
end
function bfs(g::Graph, init_nodes; reverse = false)
  order_map = []
  can_reach = init_nodes
  change = can_reach .!= 0

  connect_matrix = reverse ? g.matrix : g.matrix'
  expected_nodes_inputs_num = sum(connect_matrix', dims = 1)[1, :]
  while true
    for (i, v) in enumerate(change)
      v == true && push!(order_map, i)
    end
    last_active_nodes = can_reach
    activated = ((connect_matrix * can_reach) .== expected_nodes_inputs_num)::BitVector
    can_reach = can_reach .+ activated
    can_reach = min.(can_reach, 1)
    change = last_active_nodes .!== can_reach
    if !any(change)
      break
    end
  end
  order_map
end

out_nodes(g::Graph, node_index) = [(i, g.nodes[i]) for (i, v) in enumerate(g.matrix[node_index, :]) if v > 0]
in_nodes(g::Graph, node_index) = [(i, g.nodes[i]) for (i, v) in enumerate(g.matrix[:, node_index]) if v > 0]
function reorder!(g::Graph, order_map)
  g.matrix = g.matrix[order_map, order_map]
  g.nodes = g.nodes[order_map]
  g.states = filter(GraphFileMeta.is_state, g.nodes)
end
function recalculate_graph!(g::Graph)
  # node.id, node.index and node.op should ALL be SET!
  g.states = filter(GraphFileMeta.is_state, g.nodes)
end


function graph_apply_transform(g::Graph, indices, trans::Graph)
  mygraph::Graph = copy(g)
  mygraph.parent = g
  mygraph.patches = [GPatch(indices, trans)]
  mygraph.prealloc = empty_prealloc()
  size_g = size(g)
  @assert all([GraphFileMeta.is_io(n) for n in trans.nodes[1:2]]) "Transformation graph ordered wrong."
  @assert all([n.symbol == :out for n in trans.nodes[end:end]]) "Transformation graph ordered wrong."
  trans_core_size = length([n for n in trans.nodes if !GraphFileMeta.is_io(n)])
  trans_core::Vector{Node} = [n for n in trans.nodes if !GraphFileMeta.is_io(n)]
  max_id = maximum(n -> n.id, mygraph.nodes::Vector{Node})
  nodes0::Vector{Node} = mygraph.nodes
  res::Vector{Node} = cat(nodes0, trans_core, dims = 1)
  mygraph.nodes::Vector{Node} = deepcopy(res) # TODO optimalize
    for (i, tn) in enumerate(mygraph.nodes[end-trans_core_size+1:end])
      tn.index = size_g + i
      tn.id = max_id + i
    end
  nodes::Vector{Node} = mygraph.nodes
  trans_idxs = [indices[1:2]..., map(n -> n.index, nodes[end-trans_core_size+1:end])..., indices[end]]

  mygraph.matrix = sparse(findnz(g.matrix)..., size(mygraph), size(mygraph))
  mygraph.matrix[trans_idxs, trans_idxs] .+= trans.matrix
  mygraph.matrix = relu.(mygraph.matrix) # no negatives. (deletion on deleted)
  recalculate_graph!(mygraph)
  is_valid = order!(mygraph)
  is_valid ? mygraph : :INVALID_GRAPH
end

norm_nodes(arr) = (arr ./ sqrt.((sum(arr .^ 2, dims=[2, 3, 4, 5])) .+ 1f-4))

function get_graph_gradvalue(pre, indices = nothing)
  timesteps = size(pre, 1)
  selector_fn = d -> indices !== nothing ? d[indices] : d
  f_s_3 = stack((stack(pre[3][t] |> selector_fn, 1) for t = 1:timesteps), 4) |> norm_nodes
  f_s_4 = stack((stack(pre[4][t] |> selector_fn, 1) for t = 1:timesteps), 4) |> norm_nodes
  stacked = stack([f_s_3, f_s_4], 5)
  @assert size(stacked)[end] == 1
  stacked[.., 1]  # nodes, node_data..., 2(grad&value)
end


end  # modul GraphMethods
#%%
