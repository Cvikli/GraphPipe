module GraphPrmsFunc
using Flux.Optimise: update!, ADAM, Descent
using Zygote
using CUDA
using HwAllocator: move2hw


function inc_vec(arr)
  arr .+= 1
  return arr
end
Zygote.@adjoint function inc_vec(arr)
  arr .+= 1
  function back(dy)
    arr .-= 1
    return dy
  end
  return arr, back
end


mutable struct LayerNoBias
  w::Array{Float32}
end
mutable struct Layer
  w::Array{Float32}
  b::Array{Float32}
end
(l::Layer)(x) = x * l.w .+ l.b  # TODO BIAS lerontotta a traint?
(l::LayerNoBias)(x) =  x * l.w  # TODO BIAS lerontotta a traint?

function get_layer_nobias(x, prms, prm_counter::Vector{Int64}, init_fn::Function, hidden_size::Integer; bias_initfn=nothing)
  s = size(x)[end]
  w = get_prm_2D(prms, prm_counter, init_fn, shape=(s, hidden_size))
  return LayerNoBias(w)
end
function get_layer_bias(x, prms, prm_counter::Vector{Int64}, init_fn::Function, hidden_size::Integer; bias_initfn=nothing)
  s = size(x)[end]
  w = get_prm_2D(prms, prm_counter, init_fn, shape=(s, hidden_size))
  b = get_prm_2D(prms, prm_counter, init_fn, shape=(1, hidden_size))
  b = b .* ones(eltype(b), size(s)[1:end-1]..., 1)
  return Layer(w, b)
end
function get_prm_2D(prms, prm_counter, init_fn; shape)
  w = get_prm(prms, prm_counter, init_fn, shape=length(shape) == 3 ? shape : (shape..., 1))
  return reshape(w, size(w)[1:2]...)
end
function get_prm(prms, i::Vector{Int64}, init; shape)
  i = inc_vec(i)
  return get_prm(prms, i[1], init, shape=shape)
end

function get_prm(prms::Vector{Array{Float32,3}}, name::Integer, init_fn::Function; shape::Union{NTuple{N,Int}, Function}) where {N}
  return prms[name]
end

# function get_prm(prms::Array, name::Int, init_fn::Function; shape::Union{NTuple{M,Int} where M, Function})
#   return prms[name]
# end
function get_prm(prms::Dict{Int64, T}, name::Integer, init_fn::Function; shape::Union{NTuple{N,Int}, Function}) where {N, T}
  @assert name != -1 "Get params got -1 index!" # TODO later remove it.
  if haskey(prms, name)
    shape = typeof(shape) <: Function ? shape() : shape
    if shape != size(prms[name])
      @info "WARNING shapes doesn't match. Name: $(name) Shape old/new: $(repr(size(prms[name])))/$(repr(shape))"
      prms[name] = init_fn(shape)
    end
  else
    shape = typeof(shape) <: Function ? shape() : shape
    prms[name] = init_fn(shape)
  end
  return prms[name]::T
end

GeneralArrayofArray = Union{Vector{Array{Float32,3}}, Vector{AbstractArray{Float32,3}}, Vector{CuArray{Float32,3}}}
function update_pro!(opt::Union{ADAM,Descent}, 
                    prms::GeneralArrayofArray, 
                    grads::GeneralArrayofArray,
                    idxs=nothing)
  for (i, n) in enumerate(grads)
    if idxs !== nothing && !(i in idxs)
      continue
    end
    if n === nothing
      @show "$(i)th grad was nothing..."
      continue
    end
    update!(opt, prms[i], grads[i]) # TODO why we need grads |> move2hw? :O
  end
end

function update_pro!(opt::Union{ADAM,Descent}, prms::Union{Array{CuArray{Float32,N},1}, Array{Array{Float32,N},1}}, grads, idxs=nothing) where N
  @show " eh THIS"
  for (i, n) in enumerate(grads)
    if idxs !== nothing && !(i in idxs)
      continue
    end
    if n === nothing
      @show "$(i)th grad was nothing..."
      continue
    end
    update!(opt, prms[i], grads[i])
  end
end

end  # module PrmsContainer
#%%
