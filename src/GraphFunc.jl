module GraphFunc

using Flux, EllipsisNotation
using FastClosures
using Random
using Distributions
using HwAllocator: hw_rand, hw_fill, hw_ones, move2hw

using ..GraphPrmsFunc: get_prm

init_trunc_norm(mean, std, low, high; seed=nothing) = (shape) -> begin
  Float32.(reshape(rand(MersenneTwister(seed), truncated(Normal(mean, std), low, high), prod(shape)), shape...)) |> move2hw
end
init_trunc_norm(mean, std, plusminus; kw...) = init_trunc_norm(mean, std, mean - plusminus, mean + plusminus; kw...)
init_norm(mean, std; seed=nothing) = (shape) -> (Float32.(reshape(rand(MersenneTwister(seed), Normal(mean, std), prod(shape)), shape...)) |> move2hw)

w_init(;kw...) = init_trunc_norm(0.01, 0.02, 0.051; kw...)
state_init(;kw...) = init_trunc_norm(0.08, 0.02, 0.7, 0.9; kw...)
# w_init = (shape) -> fix_data(shape)
# w_linear_init = (shape) -> fix_data(shape) # TODO use this for testing.
w_linear_init_seeded(;kw...) = init_trunc_norm(0.0, 0.02, 0.1; kw...)
w_linear_init = init_trunc_norm(0.0, 0.02, 0.1)
w_rec_init(;kw...) = init_trunc_norm(2.2, 0.2, 1.5, 3; kw...)

####### FLUX things...
init_const(val::Float32) = (shape) -> hw_fill(val, shape)
init_const(val::Float64) = init_const(Float32(val))


function sumiter!(acc, arrs)  # TODO julia 1.6 has sum/prod/min/max with init!
  for arr in arrs
    acc .+= arr
  end
  acc
end
function prod_list(acc, arr) # TODO any is not the best idea
  acc .= arr[1]
  @views for v in arr[2:end]
    acc .*= v
  end
end
function prod_list_exclude!(acc, arr, i)
  if (length(arr) == 1)
    return hw_ones(Float32, size(arr[1])...)
  end
  acc .= i == 1 ? arr[2] : arr[1] # TODO ones create preallocate.
  start_i = i == 1 ? 3 : 2
  for vi = start_i:length(arr)
    if i == vi
      continue
    end
    acc .*= arr[vi]
  end
  acc
end


function param_op(acc, prms, X, node)
  var_shape = (1, size(X, 2), 1)  # 1: Batch 2: Parallel dataset for multiple user 3: features... 
  if node.options !== nothing && node.options.initConst !== nothing
    w = get_prm(prms, node.var_index, init_const(node.nt.initConst), shape = var_shape)
  else
    w = get_prm(prms, node.var_index, w_init(;seed=node.id), shape = var_shape)
  end
  acc .= w .* ones(Float32, size(X, 1), (1 for i = 1:ndims(X)-1)...)  # TODO repeat MANY MEMÓRIA ALLOCATION EACH TIME!
  f = let node = node;
    (∂acc, ∂prms, dy) -> (∂prms[node.var_index] .+= sum(dy, dims = 1))
  end
  return f
end

function inp_op(acc, prms, inputs, node)
  slice_s = node.options.sliceStart + 1
  slice_e = slice_s
  acc .= view(inputs, .., slice_s:slice_s)  ## TODO .. operations can be dangerous
  return (∂acc, ∂prms, dy) -> nothing
end
function state_op(acc, X, state, state_counter, prms, node_ins, node)
  state_counter .+= 1
  acc .= state[state_counter[1]]
  f = let index = state_counter[1];
    function fn(∂acc, ∂prms, dy) # TODO it should be empty. Right?
      # ∂acc[index] .+= dy
    end
    fn
  end
  return f
end
z_state_op = state_op
function prod_op(acc, node_in)
  prod_list(acc, node_in)
  f = let node_in = node_in;
     (∂acc, ∂prms, dy) ->
    for (i, v) in enumerate(node_in) ## TODO 3 nodein-ig valsz az exclude mindig gyorsabb...
      # if (all(v .!= 0))
      #   ∂acc[i] .*= dy .* acc ./ v
      # else
      tmp_val = zero(∂acc[i])
      prod_list_exclude!(tmp_val, node_in, i)
      ∂acc[i] .+= dy .* tmp_val
      # end
    end
  end
  return f
end
MUL_SOL = 0
function mul_op(acc, prms, node_in, node)
  sumiter!(acc, node_in) # amúgy ezt is egy egységben kéne a szorzással?
  acc .*= get_prm(prms, node.var_index, w_init(seed=node.id), shape = () -> (1, size(node_in[1],2), 1))
  pb = let node_in=node_in
    (∂acc, ∂prms, dy) -> begin
      for i = 1:length(node_in)
        ∂acc[i] .+= dy .* prms[node.var_index]
      end
      for i in 1:length(node_in)
        ∂prms[node.var_index] .+= sum(node_in[i] .* dy, dims = 1)
      end
    end
  end
  return pb
end
function rmul_op(acc, prms, node_in, node)
  sumiter!(acc, node_in) # amúgy ezt is egy egységben kéne a szorzással?
  prm = get_prm(prms, node.var_index, w_rec_init(;seed=node.id), shape = () -> (1, size(node_in[1],2), 1))
  acc ./= prm
  pb = let node_in=node_in
    (∂acc, ∂prms, dy) -> begin
      for i = 1:length(node_in)
        ∂acc[i] .+= dy ./ prms[node.var_index]
      end
      sum_val = sumiter!(zero(node_in[1]), node_in)
      ∂prms[node.var_index] .-= sum(sum_val .* dy, dims = 1) ./ prm ./ prm  # TODO dy kívülre
    end
  end
  return pb
end
function sum_op(acc, X, state, state_counter, prms, node_in, node)
  sumiter!(acc, node_in)
  return @closure (∂acc, ∂prms, dy) -> for i = 1:length(node_in)
    ∂acc[i] .+= dy
  end
end

function out_op(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  return @closure (∂acc, ∂prms, dy) -> for i = 1:length(node_in)
    ∂acc[i] .+= dy
  end
end
mul_op_fn(acc, X, state, state_counter, prms, node_in, node) = mul_op(acc, prms, node_in, node)
prod_op_fn(acc, X, state, state_counter, prms, node_in, node) = prod_op(acc, node_in)
function abs_op_fn(acc, X, state, state_counter, prms, node_in, node)
  sumval = zero(acc);
  sumiter!(sumval, node_in);  
  acc .= abs.(sumval);
  return @closure (∂acc, ∂prms, dy) -> begin 
    back_val = rev_abs_trans.(sumval) .* dy
    for i = 1:length(node_in)
      ∂acc[i] .+= back_val
    end
  end
end
rmul_op_fn(acc, X, state, state_counter, prms, node_in, node) = rmul_op(acc, prms, node_in, node)
function relu_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= relu.(acc);
  return @closure (∂acc, ∂prms, dy) -> begin
      sum_val = sumiter!(zero(node_in[1]), node_in)
      back_val = rev_relu_trans.(sum_val) .* dy
      for i = 1:length(node_in)
        ∂acc[i] .+= back_val
      end
    end
end
function leaky_fn(acc, X, state, state_counter, prms, node_in, node)
  sumiter!(acc, node_in);
  alpha = 0.01f0;
  acc .= leakyrelu.(acc, alpha);
  return let alpha = alpha
    (∂acc, ∂prms, dy) -> begin 
      sum_val = sumiter!(zero(node_in[1]), node_in)
      back_val = rev_leakyrelu_trans.(sum_val, alpha) .* dy
      for i = 1:length(node_in)
        ∂acc[i] .+= back_val
      end
    end
  end
end
function sigm_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= σ.(acc);
  return @closure (∂acc, ∂prms, dy) -> begin 
    back_res = dy .* acc .* (1f0 .- acc)
    for i = 1:length(node_in)
      ∂acc[i] .+= back_res
    end
  end
end
function min_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  acc .= min.(node_in...);
  return @closure (∂acc, ∂prms, dy) -> begin 
    for i = 1:length(node_in)
      ∂acc[i] .+= dy .* (node_in[i] .== acc)
    end
  end
end
function max_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  acc .= max.(node_in...);
  return @closure (∂acc, ∂prms, dy) -> begin 
    for i = 1:length(node_in)
      ∂acc[i] .+= dy .* (node_in[i] .== acc)
    end
  end
end
function soft_op_fn(acc, X, state, state_counter, prms, node_in, node)
  sumval = zero(acc);
  sumiter!(sumval, node_in);
  acc .= softsign.(sumval);
  return @closure (∂acc, ∂prms, dy) -> begin
    back_res = dy ./ ((1f0 .+ abs.(sumval)) .^ 2)
    for i = 1:length(node_in)
      ∂acc[i] .+= back_res
    end
  end
end
function Linear_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= dot(node_in, get_prm(prms, node.var_index, w_linear_init(;seed=node.id), (node_in.shape[end-1], node.nt.output_size)));
  return @closure (∂acc, ∂prms, dy) -> ∂acc .= nothing
end
function Mean_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  acc .= mean(node_in)
  return @closure (∂acc, ∂prms, dy) -> ∂acc .= nothing
end
function Std_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  acc .= std(node_in)
  return @closure (∂acc, ∂prms, dy) -> ∂acc .= nothing
end
function Flat_op_fn(acc, prms, node_ins, node) 
  acc .= reshape(node_in, (size(node_in)[1], -1), node.kwargs...);
  return @closure (∂acc, ∂prms, dy) -> ∂acc .= nothing
end
function exp_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= exp.(acc);
  return @closure (∂acc, ∂prms, dy) -> for i = 1:length(node_in)
      ∂acc[i] .+= acc .* dy
    end
end
function sqrt_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumval = zero(acc);
  sumiter!(sumval, node_in);
  acc .= sqrt.(abs.(sumval) .+ 1f-4);
  return @closure (∂acc, ∂prms, dy) -> begin
    sumval .= rev_abs_trans.(sumval) .* dy .* 0.5f0 ./ sqrt.(abs.(sumval) .+ 1f-4)
    for i = 1:length(node_in)
      ∂acc[i] .+= sumval
    end
  end
end
function neg_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= -acc;
  return @closure (∂acc, ∂prms, dy) -> (for i = 1:length(node_in) ∂acc[i] .-= dy end)
end
function inp_op_fn(acc, X, state, state_counter, prms, node_ins, node) 
  inp_op(acc, prms, X, node)
end
function cnst_op_fn(acc, X, state, state_counter, prms, node_ins, node) 
  acc .= hw_ones(Float32, size(X)[1:end-1]..., 1); 
  #TODO the backpropagation of cnst ran a little bit too few times?
  return @closure (∂acc, ∂prms, dy) -> (nothing)
end
function prm_op_fn(acc, X, state, state_counter, prms, node_ins, node) 
  param_op(acc, prms, X, node)
end
function smax_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= softmax.(acc, axis = -1);
  return (∂acc, ∂prms, dy) -> println("Unimplemented!")
end
function trans_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumiter!(acc, node_in);
  acc .= batched_transpose(node_in, axes = node.nt.axes);
  return (∂acc, ∂prms, dy) -> println("Unimplemented!")
end
function reci_op_fn(acc, X, state, state_counter, prms, node_in, node) 
  sumval = zero(acc);
  sumiter!(sumval, node_in);
  acc .= 1f0 ./ (abs.(sumval) .+ 1f-4);
  return @closure (∂acc, ∂prms, dy) -> begin
    sumval .= rev_abs_trans.(sumval) .* dy .* -1.f0  ./ ((abs.(sumval) .+ 1f-4) .^ 2)
    for i = 1:length(node_in)
      ∂acc[i] .+= sumval
    end
  end
end


rev_relu_trans(d) = d >= 0.f0 ? 1.f0 : 0f0
rev_abs_trans(d) = d >= 0.f0 ? 1.f0 : -1.f0
rev_leakyrelu_trans(d, a) = d >= 0.f0 ? 1.f0 : a

forward_backward_2_fluxop = (
  out = out_op,
  sum = sum_op,
  mul = mul_op_fn,
  prod = prod_op_fn,
  abs = abs_op_fn, 
  rmul = rmul_op_fn,
  relu = relu_op_fn,
  leaky = leaky_fn,
  sigm = sigm_op_fn, # sigmoid
  min = min_op_fn, # min
  max = max_op_fn, # max
  soft = soft_op_fn,
  Linear = Linear_op_fn,
  Mean = Mean_op_fn,
  Std = Std_op_fn,
  Flat = Flat_op_fn,
  exp = exp_op_fn,
  sqrt = sqrt_op_fn,
  neg = neg_op_fn,
  # state ops are different
  state = state_op,
  z_state = z_state_op,
  # input ops are different
  inp = inp_op_fn,
  cnst = cnst_op_fn,
  prm = prm_op_fn,
  smax = smax_op_fn,
  trans = trans_op_fn,
  reci = reci_op_fn,  
  # noop,
  # maxpool,
  # conv,
)


end
