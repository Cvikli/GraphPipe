module GraphFileMeta

using ..GraphWorld: Node

io_symbols = [:inp, :out]
var_symbols = [:mul, :rmul, :prm, :Linear, :state]
state_learnable_symbols = [:state, ]
state_symbols = [:state, :z_state]
all_symbols = [io_symbols..., var_symbols..., :sum, :prod, :abs, :relu, :leaky, :sigm, :soft, :smax, :trans, :Mean, :Std, :Flat, :exp, :sqrt, :reci, :neg, state_symbols...]
is_state(n::Node)::Bool = n.symbol ∈ state_symbols
is_state_learnable(n::Node)::Bool = n.symbol ∈ state_learnable_symbols
is_var(n::Node)::Bool = n.symbol ∈ var_symbols
is_io(n::Node)::Bool = n.symbol ∈ io_symbols


op_str2symbol = Dict{String,Symbol}(
  "Sum" => :sum,
  "Output" => :out,
  "Input" => :inp,
  "Param" => :prm,
  "1" => :cnst,
  "Add" => :sum,
  "Mul" => :mul,
  "Rmul" => :rmul,
  "Prod" => :prod,
  "Recurrent" => :state,
  "Reciproc" => :reci,
  "Relu" => :relu,
  "Leaky" => :leaky,
# firebase dataformat support
  "s" => :sum,
  "p" => :prod,
  "*" => :mul,
  "/*" => :rmul,
  "R" => :state,
  "State" => :state,
  "ZR" => :z_state,
  "relu" => :relu,
  "leaky" => :leaky,
  "soft" => :soft,
  "lnR" => :sigm,
  "sigmoid" => :sigm,
  "-" => :neg,
  "abs" => :abs,
  "min" => :min,
  "max" => :max,
  "exp" => :exp,
  "sqrt" => :sqrt,
  "1/x" => :reci,
)
# sum(X, [1,2])
min_op_input_num = Dict{Symbol,Int}(  # node stacking operator
  :inp => 0,
  :prm => 0,
  :cnst => 0,
  :out => 1,
  :sum => 1,
  :mul => 1,
  :prod => 1,
  :abs => 1,
  :rmul => 1,
  :relu => 1,
  :leaky => 1,
  :sigm => 1,
  :soft => 1,
  :smax => 1,
  :trans => 1,
  :Linear => 1,
  :Mean => 1,
  :Std => 1,
  :Flat => 1,
  :exp => 1,
  :sqrt => 1,
  :min => 2,
  :max => 2,
  :reci => 1,
  :neg => 1,
  :state => 1,
  :z_state => 1,
)
max_op_input_num = Dict{Symbol,Int}(:inp => 0, :cnst => 0, :prm => 0)
INFINITY = 99999
for sym in all_symbols 
  if !(sym in keys(max_op_input_num))
    max_op_input_num[sym] = INFINITY 
  end
end

sym_2_id = Dict(sym => id for (id, sym) in enumerate(all_symbols))



end
