module GraphWorld

using Parameters
using Printf
using EllipsisNotation
using SparseArrays
using Boilerplate: sizes, filter, @sizes, @typeof
import Base: show


@with_kw mutable struct Options
  sliceStart::Int32 = - 1
  initConst::Union{Function,Nothing}=nothing
end
# one_hot_encoded(nodes::Vector{Node}) = map(n -> n.symbol_onehot, nodes)
@with_kw mutable struct Node
  id::Int32
  index::Int32 = -1
  var_index::Int32 = -1
  op::Function
  # custom::Union{VarOpts,InputOpts,Nothing}
  symbol::Symbol
  # symbol_id::Int
  # symbol_onehot::Vector{Float32}
  options::Union{Options,Nothing} = nothing
end
# mutable struct InputOpts
#   sliceStart::Int32 = - 1
# end
# mutable struct VarOpts
#   var_index
  # initConst::Union{Function,Nothing}=nothing
# end
# symbol(n::Sum) = "Sum"
# symbol(n::Mul) = "Mul"
# op(n::Sum) = "Sum"
# op(n::Mul) = "Mul"

# function Base.show(io::IO, n::Node)
#   var_str = n.var_index != -1 ? ":$(n.var_index)" : ""
#   show(io, "$(n.id):$(n.index):$(n.symbol)$var_str")
# end  

# one_hot_encoded(nodes::Vector{Node}) = map(n -> n.symbol_onehot, nodes)


@with_kw struct GraphRaw
  is_learnable::Bool = false
  loss_fn::Union{Nothing,String} = "mse"
  last_edge_id::Union{Nothing,Int}
  edges::Array
  last_node_id::Union{Nothing,Int}
  nodes::Array
end

abstract type GraphBase end
@with_kw mutable struct GraphNetwork <: GraphBase
  nodes::Vector{Node} = []
  is_learnable::Bool = false
  prealloc = nothing  # TODO lejjebb definiálva van már prealloc típus...
end

struct GPatch
  indices::Vector{Int}
  transformation::GraphBase
end

empty_prealloc() = (Array{Float32, 3}[],[Array{Float32, 3}[]],[Array{Float32, 3}[]],[Array{Float32, 3}[]],[Array{Float32, 3}[]],[Function[]])

@with_kw mutable struct Graph <: GraphBase
  nodes::Vector{Node} = Node[]
  loss_fn::String = "nothing"
  states::Vector{Node} = Node[]
  matrix::Union{SparseMatrixCSC{Int32,Int32}, Nothing} = spzeros(Int, 0, 0)
  node_input_indices::Union{Nothing, Vector{Union{Nothing, Vector{Int32}}}} = nothing
  # Out indices removed since not necessary.
  # node_output_indices::Vector{Union{Nothing, Vector{Int32}}} = Vector{Union{Nothing, Vector{Int32}}}()
  is_learnable::Bool = false
  out_idx::Vector{Int} = Int[]
  is_rnn::Bool = false
  prealloc::Tuple{Vector{Array{Float32, 3}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Array{Float32, 3}}}, Vector{Vector{Function}}} = empty_prealloc() 
  confidence::Union{Nothing, Any} = nothing
# params for inheritance
  patches::Vector{GPatch} = GPatch[]
  parent::Union{Nothing, Graph} = nothing
# a way to return with validation loss.
  val_loss::Union{Nothing,Float32} = nothing
end
Base.:+(g::Graph, g2::Graph) = g2

Base.size(g::Graph) = length(g.nodes)
function Base.copy(g::Graph)
  Graph(nodes=g.nodes,
  loss_fn=g.loss_fn,
  states=g.states,
  matrix=g.matrix,
  node_input_indices=g.node_input_indices,
  is_learnable=g.is_learnable,
  out_idx=g.out_idx,
  is_rnn=g.is_rnn,
  prealloc=g.prealloc,
  confidence=g.confidence,
  patches=g.patches,
  parent=g.parent,
  val_loss=g.val_loss)
end

is_compressed(g::Graph) = g.matrix === nothing

function uncompress(g::Graph)
  parents = [g] # the list of compressed parents
  while is_compressed(parents[end])
    push!(parents, parents[end].parent)
    @assert parents[end] != nothing "No uncompressed parent for graph."
  end
  patch = g.patches[1]
  # @show (patch.indices .|> i -> g.parent.nodes[i].id, patch.transformation)
  reconstrued = deepcopy(parents[end])
  for gp in parents[end-1:-1:1]
    # @show gp.patches
    @assert length(gp.patches) == 1 "Still only 1 patch supported."
    patch = gp.patches[1]
    reconstrued = graph_apply_transform(reconstrued, patch.indices, patch.transformation)
  end
  reconstrued
end
function compress!(g::Graph; clear_params=false)
  if is_compressed(g)
    @warn "Graph already compressed!"
  end
  if g.parent === nothing
    @warn "We tried debuilding root graph."
    return g
  end
  g.prealloc = empty_prealloc()
  g.nodes = Node[]
  if clear_params
    g.params = nothing
  end
  g.matrix = spzeros(Int, 0, 0)
  g.node_input_indices= nothing
  g
end

# function Base.show(io::IO, g::Graph)
#   compact = get(io, :compact, false)
#   show(io, "Graph:$(replace(join(g.nodes, ", "),"\"" => "")),states:$([n.index for n in g.states]), edges:$(map(x->x.I, findall(x->x>0, g.matrix)))")
# end

end  # modul GraphWorld
#%%
